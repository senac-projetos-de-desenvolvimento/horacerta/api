const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const dayjs = require("dayjs");
const login = require("../middleware/login.js");

//=============== Rota de cadastro de Ponto  Rh =================
router.post("/", (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    const hora_entrada = `${dayjs().format("HH")}:${dayjs().format("mm")}`;
    const data = `${dayjs().format("YYYY")}-${dayjs().format(
      "MM"
    )}-${dayjs().format("DD")}`;

    conn.query(
      "INSERT INTO pontos (data, hora_entrada, colaboradores_idcolaboradores) VALUES (?,?,?)",
      [data, hora_entrada, req.body.colaboradores_idcolaboradores],
      (error, resultado, field) => {
        conn.release();

        error && res.status(500).send({ error: error });

        res.status(201).send({
          mensagem: "Ponto cadastrado com sucesso!",
          id: resultado.insertId,
        });
      }
    );
  });
});

//================= Retorna todos Pontos ====================

router.get("/", login, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query("SELECT pontos.num_registro, colaboradores.nome_completo, pontos.colaboradores_idcolaboradores, pontos.data, pontos.entrada, pontos.saida FROM pontos JOIN colaboradores ON colaboradores.idcolaboradores=pontos.colaboradores_idcolaboradores;", (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).send({ error: error });
      }
      res.status(201).send({ response: resultado });
    });
  });
});

//======== Retorna os pontos de um unico colaborador
router.get("/:id", login, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query (`SELECT pontos.num_registro, colaboradores.nome_completo, pontos.colaboradores_idcolaboradores, pontos.data, pontos.entrada, pontos.saida, colaboradores.idcolaboradores FROM pontos JOIN colaboradores on colaboradores.idcolaboradores=pontos.colaboradores_idcolaboradores WHERE idcolaboradores LIKE '${id}' order by data asc `, 
    [req.params.idcolaboradores],
    (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).json({ error: error });
      }
      res.status(201).json({ response: resultado });
    });
  });
});

//===================== Pesquisa por data de um ponto de um colaborador =======================
router.get("/:id/:data", login, (req, res, next) => {
  const  id = req.params.id
  const  data = req.params.data
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query (`SELECT * FROM pontos WHERE data LIKE '%${data}%' AND colaboradores_idcolaboradores = ${id}`, 
    [data],
    (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).json({ error: error });
      }
      res.status(201).json({ response: resultado });
    });
  });
});

//========= Edição de Pontos ========
router.put("/:id", login, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM pontos WHERE num_registro LIKE '${id}'`,
      [req.body.num_registro],
      (error, resultado, field) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        if (resultado.length == resultado) {
          conn.release();
          res.status(401).send({ mensagem: "Ponto não encontrado!" });
        } else {
          conn.query(
            `UPDATE pontos SET data = ?, entrada = ?, saida = ? WHERE num_registro LIKE '${id}'`,
            [req.body.data, req.body.entrada, req.body.saida, req.body.num_registro],
            (error, resultado, field) => {
              conn.release();
              if (error) {
                return res.status(500).send({ error: error });
              }
              res.status(202).send({
                mensagem: "Ponto Editado com sucesso!",
              });
            }
          );
        }
      }
    );
  });
});

//======== Exclusão de Pontos ==========
router.delete("/", login, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM pontos WHERE num_registro = ?`,
      [req.body.num_registro],
      (error, resultado, field) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        if (resultado.length == resultado) {
          conn.release();
          res.status(401).send({ mensagem: "Ponto não encontrado!" });
        } else {
          conn.query(
            `DELETE FROM pontos WHERE num_registro = ?`,
            [req.body.num_registro],
            (error, resultado, field) => {
              conn.release();
              if (error) {
                return res.status(500).send({ error: error });
              }
              res.status(202).send({
                mensagem: "Ponto removido com sucesso!",
              });
            }
          );
        }
      }
    );
  });
});

//========= Relatorio De Horas trabalhadas  ===========
router.get("/relatorio/:id/:dataInicio/:dataFinal",  (req, res, next) => {
  
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    const  id  = req.params.id;
    let dataInicio = req.params.dataInicio + " 00:00:00";
    let dataFinal = req.params.dataFinal + " 23:59:59";

    conn.query(
      `SELECT  pontos.num_registro, pontos.colaboradores_idcolaboradores, pontos.data, pontos.entrada, pontos.saida, colaboradores.nome_completo, colaboradores.horas_mensais FROM pontos JOIN colaboradores ON colaboradores.idcolaboradores=pontos.colaboradores_idcolaboradores  WHERE data between ? AND ? AND colaboradores_idcolaboradores = ${id} AND saida IS NOT NULL `,
      [dataInicio, dataFinal],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).json({ error: error });
        }
        let hora = 0;
        for (let i = 0; i < resultado.length; i++) {
          const hora_saida = resultado[i].saida;
          const partes = hora_saida.split(":");
          const fracao_saida = partes[0] + partes[1];
          const hora_entrada = resultado[i].entrada;
          const partes_entrada = hora_entrada.split(":");
          const fracao_entrada = partes_entrada[0] + partes_entrada[1];
          hora += fracao_saida - fracao_entrada;
        }
        const total = (hora * 60) / 1000;
        const total_mes = total / 6;

        const total_horas_mes = total_mes.toFixed(2);
        res.status(201).json({
          response: {
            resultado,
            total_horas_mes,
          },
        });
      }
    );
  });
});

module.exports = router;
