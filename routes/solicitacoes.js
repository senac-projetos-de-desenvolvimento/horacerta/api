const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const login = require("../middleware/login.js");



//===== Retorna todas Solicitações ==========
router.get("/", login, (req, res, nex) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(`SELECT pontos.data AS pontosData, pontos.entrada AS pontosEntrada, pontos.saida AS pontosSaida, solicitacoes.entrada, solicitacoes.saida, solicitacoes.id, .solicitacoes.pontos_num_registro, solicitacoes.data, solicitacoes.observacao, solicitacoes.edit, colaboradores.nome_completo FROM solicitacoes JOIN colaboradores JOIN pontos ON colaboradores.idcolaboradores=solicitacoes.colaboradores_idcolaboradores AND solicitacoes.pontos_num_registro=pontos.num_registro order by data asc;`, 
    (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).send({ error: error });
      }
      res.status(201).send({ response: resultado });
    });
  });
});

//========== Retorna as Solicitações de um único colaborador ===========
router.get("/:id", login, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM solicitacoes WHERE colaboradores_idcolaboradores LIKE ${id}`,
      [req.body.colaboradores_idcolaboradores],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(201).send({ response: resultado });
      }
    );
  });
});

//======= Update para Aceita =======
router.put("/:id", login, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `UPDATE solicitacoes SET edit = ? WHERE id LIKE '${id}'`,
      [req.body.edit],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(202).send({ response: "Solicitação aceita" });
      }
    );
  });
});

//====== Exclusão de Solicitação ========
router.delete("/:id_request", login, (req, res, next) => {
  const { id_request } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `
    DELETE FROM solicitacoes WHERE id LIKE ${id_request}`,
      [req.body.id],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(202).send({
          mensagem: "Solicitação removida com sucesso!",
        });
      }
    );
  });
});

module.exports = router;
