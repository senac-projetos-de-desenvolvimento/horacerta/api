const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const dayjs = require("dayjs");
const loginTwo = require("../middleware/loginTwo.js");
const login = require("../middleware/login.js");

//========= Cadastro de Tarefas/Projetos =========
router.post("/", loginTwo, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    const hora = `${dayjs().format("HH")}:${dayjs().format("mm")}`;

    const data_inicio = `${dayjs().format("YYYY")}-${dayjs().format(
      "MM"
    )}-${dayjs().format("DD")}`;

    conn.query(
      `INSERT INTO tarefas_projetos (data_inicio, hora, nome, descricao, colaboradores_idcolaboradores) VALUES (?,?,?,?,?)`,
      [
        data_inicio,
        hora,
        req.body.nome,
        req.body.descricao,
        req.body.colaboradores_idcolaboradores,
      ],
      (error, resultado, field) => {
        

        error && res.status(500).send({ error: error });

        res.status(201).send({
          mensagem: "Tarefa Cadastrada",
        });
      }
    );
    conn.release();
  });
});

//========= Retorna todas Tarefas/Projetos =========
router.get("/", login, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(`SELECT * FROM tarefas_projetos;`, (error, resultado, field) => {
      conn.release();
      if (error) {
        return res.status(500).send({ error: error });
      }
      res.status(201).send({ response: resultado });
    });
  });
});

//========= Finaliza Tarefas/Projetos ============
router.put("/", loginTwo, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    const hora_termino = `${dayjs().format("HH")}:${dayjs().format("mm")}`;

    const data_termino = `${dayjs().format("YYYY")}-${dayjs().format(
      "MM"
    )}-${dayjs().format("DD")}`;
    conn.query(
      `UPDATE tarefas_projetos SET data_termino = ?, hora_termino = ? WHERE id_tarefas_projetos = ?`,
      [data_termino, hora_termino, req.body.id_tarefas_projetos],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(201).send({ mensagem: "Tarefa finalizada!" });
      }
    );
  });
});

//======= Retorna todas Tarefas/Projetos de um único colaborador =========
router.get("/:id", loginTwo, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM tarefas_projetos WHERE colaboradores_idcolaboradores LIKE ${id}`,
      [req.body.colaboradores_idcolaboradores],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        if (resultado.length === resultado) {
          return res
            .status(401)
            .send({ mensagem: "Colaborador não encontrado" });
        }
        res.status(201).send({ response: resultado });
      }
    );
  });
});

//========= Edita campos de Tarefas/Projetos ===========
router.patch("/editar", loginTwo, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `UPDATE tarefas_projetos SET nome = ?, descricao = ? WHERE id_tarefas_projetos = ?`,
      [req.body.nome, req.body.descricao, req.body.id_tarefas_projetos],
      (errror, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(201).send({ mensagem: "Compos editados!" });
      }
    );
  });
});

//=========== Exclusão de Tarefas/Projetos ==============
router.delete("/:id", loginTwo, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `DELETE FROM tarefas_projetos WHERE id_tarefas_projetos LIKE ${id}`,
      [req.body.id_tarefas_projetos],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(202).send({
          mensagem: "Tarefa/Projeto removido com sucesso!",
        });
      }
    );
  });
});

module.exports = router;
