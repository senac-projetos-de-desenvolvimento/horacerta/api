const express = require("express");
const router = express.Router();
const mysql = require("../mysql").pool;
const bcrypt = require("bcrypt");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const login = require("../middleware/login.js");
const loginTwo = require("../middleware/loginTwo.js");

//============================================== ROTAS RH =====================================

//=============== Login do RH =============== || RH
router.post("/login", cors(), (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    const query = `SELECT * FROM colaboradores WHERE cpf = ?`;
    conn.query(query, [req.body.cpf], (error, results, fields) => {
      conn.release();
      if (error) {
        return res.status(500).send({ error: error });
      }
      if (results.length < 1) {
        return res.status(401).send({ mensagem: "Falha na autenticação" });
      }
      bcrypt.compare(req.body.senha, results[0].senha, (err, result) => {
        console.log(results[0].setor);
        if (results[0].setor !== "RH") {
          return res.status(401).send({ mensagem: "Falha na autenticação!" });
        }
        if (err) {
          return res.status(401).send({ mensagem: "Falha na autenticação!" });
        }
        if (result) {
          const token = jwt.sign(
            {
              idcolaboradores: results[0].idcolaboradores,
              setor: results[0].setor,
            },
            process.env.JWT_KEY,
            {
              expiresIn: "60d",
            }
          );

          return res.status(200).send({
            mensagem: "Autenticado com Sucesso!",
            token: token,
            setor: results[0].setor,
            id: results[0].idcolaboradores,
          });
        }
        return res.status(401).send({ mensagem: "Falha na autenticação!" });
      });
    });
  });
});

//============== Cadastro dos colaboradores ============= || RH
router.post("/", login, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      "SELECT * FROM colaboradores WHERE cpf = ?",
      [req.body.cpf],
      (error, resultado, field) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        if (resultado.length > 0) {
          conn.release();
          res.status(401).send({ mensagem: "Usuario já cadastrado" });
        } else {
          bcrypt.hash(req.body.senha, 10, (errBcrypt, hash) => {
            if (errBcrypt) {
              return res.status(500).send({ error: errBcrypt });
            }
            conn.query(
              "INSERT INTO colaboradores (nome_completo, cpf, email, data_nasc, endereco, numero, data_admissao, telefone_celular, setor, senha, horas_mensais) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
              [
                req.body.nome_completo,
                req.body.cpf,
                req.body.email,
                req.body.data_nasc,
                req.body.endereco,
                req.body.numero,
                req.body.data_admissao,
                req.body.telefone_celular,
                req.body.setor,
                hash,
                req.body.horas_mensais,
              ],
              (error, resultado, field) => {
                conn.release();
                if (error) {
                  return res.status(500).send({ error: error });
                }
                res.status(201).send({
                  mensagem: "Usuario cadastrado com sucesso",
                  id: resultado.insertId,
                });
              }
            );
          });
        }
      }
    );
  });
});

//=============== Retorna todos Colaboradores ================ || RH
router.get("/", login, (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM colaboradores order by nome_completo asc;`,
      (error, resultado, field) => {
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(201).send({ response: resultado });
      }
    );
    conn.release();
  });
});

//============ Pesquisa por nome ou por id ================== || RH
router.get("/:palavra", login, cors(), (req, res, next) => {
  const { palavra } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM colaboradores WHERE idcolaboradores = ${palavra};`,
      [],
      (error, resultado, fields) => {
        if (error || !resultado.length) {
          conn.query(
            `SELECT * FROM colaboradores WHERE nome_completo LIKE "%${palavra}%"`,
            [],
            (error, resultado, field) => {
              if (error) {
                return res.status(500).json({ error: error });
              } else {
                return res.status(200).json({ response: resultado });
              }
            }
          );
        } else {
          return res.status(201).json({ response: resultado });
        }
      }
    );
    conn.release();
  });
});

//======================= Rota de Exclusão Colaborador ============== || RH
router.delete("/:id", login, (req, res, next) => {
  const { id } = req.params;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      `DELETE FROM colaboradores WHERE idcolaboradores LIKE '${id}'`,
      [req.body.idcolaboradores],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(202).send({
          mensagem: "Colaborador Removido com Sucesso!",
          id: resultado.insertId,
        });
      }
    );
  });
});

//============== Rota de Edição de dados do colaborador ========= || RH
router.put("/:idcolaboradores", login, (req, res, next) => {
  const idcolaboradores = req.params.idcolaboradores;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    bcrypt.hash(req.body.senha, 10, (errBcrypt, hash) => {
      if (errBcrypt) {
        return res.status(500).send({ error: errBcrypt });
      }
      conn.query(
        `UPDATE colaboradores SET nome_completo = ?, email = ?, data_nasc =?, endereco = ?, numero = ?, data_admissao = ?, telefone_celular = ?, setor = ?, horas_mensais = ? WHERE idcolaboradores LIKE '${idcolaboradores}' `,
        [
          req.body.nome_completo,
          req.body.email,
          req.body.data_nasc,
          req.body.endereco,
          req.body.numero,
          req.body.data_admissao,
          req.body.telefone_celular,
          req.body.setor,
          req.body.horas_mensais,
          req.body.idcolaboradores,
        ],
        (error, resultado, field) => {
          conn.release();
          if (error) {
            return res.status(500).send({ error: error });
          }
          res.status(202).send({
            mensagem: "Usuario atualizado com sucesso!",
            id: resultado.insertId,
          });
        }
      );
    });
  });
});

//========== Retorna só um colaborador ============= || RH
router.get("/id/:id", loginTwo, (req, res, next) => {
  const id = req.params.id;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    conn.query(
      `SELECT * FROM colaboradores WHERE idcolaboradores LIKE '${id}'`,
      [req.body.idcolaboradores],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).json({ error: error });
        }
        res.status(201).json({ response: resultado });
      }
    );
  });
});

//==================================================== ROTAS DOS COLABORADORES ==================================================

//=============== Edição de dados do Colaborador ============ || Colaborador
router.put("/editar/:id", loginTwo, (req, res, next) => {
  const id = req.params.id;
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    conn.query(
      `UPDATE colaboradores SET email = ?, endereco = ?, numero = ?, telefone_celular = ? WHERE idcolaboradores LIKE '${id}'`,
      [
        req.body.email,
        req.body.endereco,
        req.body.numero,
        req.body.telefone_celular,
        req.body.idcolaboradores,
      ],
      (error, resultado, field) => {
        conn.release();
        if (error) {
          return res.status(500).send({ error: error });
        }
        res.status(202).send({
          mensagem: "Usuario atualizado com sucesso!",
          id: resultado.insertId,
        });
      }
    );
  });
});

//============= Login Colaborador ============= || Colaborador
router.post("/login/colaborador", cors(), (req, res, next) => {
  mysql.getConnection((error, conn) => {
    if (error) {
      return res.status(500).send({ error: error });
    }
    const query = `SELECT * FROM colaboradores WHERE cpf = ?`;
    conn.query(query, [req.body.cpf], (error, results, fields) => {
      conn.release();
      if (error) {
        return res.status(500).send({ error: error });
      }
      if (results.length < 1) {
        return res.status(401).send({ mensagem: "Falha na autenticação" });
      }
      bcrypt.compare(req.body.senha, results[0].senha, (err, result) => {
        console.log(results[0].setor);
        if (results[0].setor === "RH") {
          return res.status(401).send({ mensagem: "Falha na autenticação!" });
        }
        if (err) {
          return res.status(401).send({ mensagem: "Falha na autenticação!" });
        }
        if (result) {
          const token = jwt.sign(
            {
              idcolaboradores: results[0].idcolaboradores,
              setor: results[0].setor,
            },
            process.env.JWT_KEY_TWO,
            {
              expiresIn: "60d",
            }
          );

          return res.status(200).send({
            mensagem: "Autenticado com Sucesso!",
            token: token,
            setor: results[0].setor,
            id: results[0].idcolaboradores,
          });
        }
        return res.status(401).send({ mensagem: "Falha na autenticação!" });
      });
    });
    
  });
});

module.exports = router;
